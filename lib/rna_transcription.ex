defmodule RnaTranscription do
  @doc """
  Transcribes a character list representing DNA nucleotides to RNA

  ## Examples

  iex> RnaTranscription.to_rna('ACTG')
  'UGAC'
  """

  @spec to_rna([char]) :: [char]
  def to_rna(dna) do
    transcription = %{
      ?A => ?U,
      ?C => ?G,
      ?G => ?C,
      ?T => ?A
    }

    Enum.map(dna, fn rna -> transcription[rna] end)
  end
end
